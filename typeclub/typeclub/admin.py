from django.contrib import admin
from typeclub.models import UserProfile, GameSession, Blog

admin.site.register(UserProfile)
admin.site.register(GameSession)
admin.site.register(Blog)
