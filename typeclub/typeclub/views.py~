from django.http import HttpResponse
from django.template import RequestContext, loader
from django.shortcuts import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from typeclub.models import UserForm, UserProfile, UserProfileForm, GameSession, Blog
from django.http import HttpResponseRedirect
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
import datetime
import os
import random

def index(request):
	return render(request, "typeclub/index.html", {"posts": Blog.objects.all().reverse()[:10]})

def about(request):
	return render(request, "typeclub/about.html", {})

def register(request):
	context = RequestContext(request)
	registered = False
	if request.method == "POST":
		uform = UserForm(data = request.POST)
		pform = UserProfileForm(request.POST, request.FILES)
		if uform.is_valid() and pform.is_valid():
			user = uform.save()
			pw = user.password
			user.set_password(pw)
			user.save()
			profile = pform.save(commit = False)
			profile.user = user
			profile.save()
			#save_file(request.FILES['picture'])
			registered = True
		else:
			print uform.errors, pform.errors
	else:
		uform = UserForm()
		pform = UserProfileForm()
	return render_to_response("typeclub/register.html", {"uform": uform, "pform": pform, "registered": registered}, context)

def save_file(file, path = ""):
	filename = file._get_name()
	fd = open('%s/%s' % (settings.MEDIA_ROOT, str(path) + str(filename)), 'wb' )
	for chunk in file.chunks():
		fd.write(chunk)
	fd.close()

def user_login(request):
	context = RequestContext(request)
	#print request
	if request.method == "POST":
		username = request.POST["username"]
		password = request.POST["password"]
		user = authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				login(request, user)
				# Redirect to page requested (default is "/")
				return HttpResponseRedirect(request.POST["next"])
			else:
				# Return a "disabled account" error message
				return HttpResponse("Your account is disabled")
		else:
			# Return an invalid login error message
			print "Invalid login details " + username + " " + password
			return render_to_response("typeclub/login.html", {}, context)
	else: 
		n = None
		if "next" in request.GET:
			n = request.GET["next"]
		# The login is a GET request, just show the user the login form
		return render_to_response("typeclub/login.html", {"next": n}, context)

@login_required
def user_logout(request):
	context = RequestContext(request)
	logout(request)
	# Redirect
	return HttpResponseRedirect("/")

def howto(request):
	return render(request, "typeclub/howto.html", {})

@login_required
def profile(request):
	context = RequestContext(request)
	u = User.objects.get(username = request.user)
	try:
		up = UserProfile.objects.get(user = u)
		# get ordered list of users, ranked by wpm
		tt = UserProfile.objects.all().order_by('wpm').reverse()
		# calculate this user's rank in the list
		tr = calculate_rank(up,tt)
		# show only top ten
		tt = tt[:10]		
	except:
		up = None
		tt = None
		tr = None
	return render_to_response("typeclub/profile.html", {"user":u, "userprofile": up, 
				  "toptype": tt, "typerank": tr}, context)

# This function calculates a user's rank given a user and a list
# It sees users with the same wpm as having the same rank
def calculate_rank(user,data):
	if (len(data) > 0):
		if data[0] == user:
			return 1
	count = 1
	i = 0
	while (i < (len(data)-1)):
		if data[i].wpm != data[i+1].wpm:
			count += 1
		if data[i+1] == user:
			return count
		i += 1
	return None 

@login_required
def play(request):
	template = loader.get_template("typeclub/play.html")

	# If user1 is requesting to create a gamesession but one already exists
	if (GameSession.objects.filter(user1 = User.objects.get(username = request.user)).exists()):
		return render(request, "typeclub/find_game.html", {})

	# If user2 is requesting to join a gamesession he is in
	if (GameSession.objects.filter(user2 = User.objects.get(username = request.user)).exists()):
		gs = GameSession.objects.get(user2 = User.objects.get(username = request.user))
		return render(request, "typeclub/play.html", {"gs": gs})

	# Request must then be user1 looking to create a gamesession
	new = GameSession(user1 = User.objects.get(username = request.user))
	new.save()
	context_dict = {"gs": new, "user": request.user}
	context = RequestContext(request, context_dict)
	return HttpResponse(template.render(context))

# Either user1 or user2 can call poll
# When user 2 calls, need to stop user1 polling further and begin the game
@csrf_exempt
@login_required
def poll(request):
	# user1
	if (GameSession.objects.filter(user1 = User.objects.get(username = request.user)).exists()):
		gs = GameSession.objects.get(user1 = User.objects.get(username = request.user))
		if (gs.user2 is None):
			return HttpResponse("nr")
		return HttpResponse("r")
	# user2, starting game
	if (GameSession.objects.filter(user2 = User.objects.get(username = request.user)).exists()):
		return HttpResponse("r")
	# GameSession may have been deleted
	return HttpResponse("na")

@login_required
def find_game(request):
	template = loader.get_template('typeclub/find_game.html')

	# If request.user already is in a lobby, deletes the game session so they can join another game
	if (GameSession.objects.filter(user1 = User.objects.get(username = request.user)).exists()):
		gs = GameSession.objects.get(user1 = User.objects.get(username = request.user))
		gs.delete()

	# Returns a list of possible games to join
	gs = GameSession.objects.filter()
	context_dict = {'games': gs}
	context = RequestContext(request, context_dict)
	return HttpResponse(template.render(context))

@csrf_exempt
@login_required
def join_gamesession(request):
	if request.method == "POST":
		# Must be get, if using filter cannot update!
		gs = GameSession.objects.get(id = request.POST["id"])
		user = User.objects.get(username = request.user)
		if gs.user2 is not None:
			return render(request, "typeclub/find_game.html")
		gs.user2 = user
		gs.save()
		return HttpResponseRedirect("/play/")
	return HttpResponse("BAD")

@csrf_exempt
@login_required
def leave_game(request):
	context = RequestContext(request)
	if request.method == "POST":
		sid = request.POST['id']
		su1 = request.POST['u1']
		su2 = request.POST['u2']
		user = request.user
		gs = GameSession.objects.get(id=sid)
		if su1 == user.username:
			gs.delete()
		if su2 == user.username:
			gs.user2 = None
			gs.save()
	return HttpResponseRedirect("/")

@csrf_exempt
def generate_words(request):
	if (request.method == "POST"):
		# Lock so only one user generates the words and saves, other user waits till the words are generated
		while(GameSession.objects.get(id = request.POST["id"]).lock == True):
			continue
		getGs = GameSession.objects.get(id = request.POST["id"])
		getGs.lock = True
		getGs.save()
		# Store user1 time
		if (getGs.user1 == request.user):
			getGs.u1start = datetime.datetime.now()
		# Store user2 time
		else:
			getGs.u2start = datetime.datetime.now()
		# First user that gets here, generate the words
		if (getGs.words == ""):
			words = [line.strip() for line in open(os.getcwd()+'/static/enable1.txt')]
			dingledongs=""
			for i in range(8):
				dingledongs+=random.choice(words)
				if (i != 7):
					dingledongs += " "
			getGs.words = dingledongs
			getGs.lock = False
			getGs.save()
			return HttpResponse(getGs.words)
		# Second user just gets the words generated
		else:	
			getGs.lock = False
			getGs.save()
			return HttpResponse(getGs.words)
	return

# check the user's typed words 
@csrf_exempt
def check(request):
	if request.is_ajax():
		if request.method == "POST":
			getGs = GameSession.objects.filter(id = request.POST["id"])
			# if the session was closed, the player has either lost or
			# the session encountered a problem.
			if ((getGs is None) or (not getGs)): 
				return HttpResponse("Game lost")
			words = getGs[0].words
			# strip off trailing ends
			userWords = request.POST["textBox"].rstrip()
			if (words == userWords):
				return establish_winner(request)
			else:
				return HttpResponse("Spelling error")
	return HttpResponse("BAD REQUEST")

# establish which user in the session has won
def establish_winner(request):
	gs = GameSession.objects.get(id = request.POST["id"])
	# sometimes a broken pipe is thrown and datetime.now() fails
	# to be loaded in the session. In this case, discard the game
	if ((gs.u1start is None) or (gs.u2start is None)):
		return HttpResponse("Error starting timer in game")
	# case session.user1 finished typing
	if request.user == gs.user1:
		# check if first time calling this 
		if (gs.u1end is None):
			# if the other user is still typing, we need more data
			if (gs.u2end is None):
				gs.u1end = datetime.datetime.now()
				gs.save()
				# if this user started after the other one, the other one
				# cannot possibly be faster
				if gs.u1start > gs.u2start:
					gs = GameSession.objects.get(id = request.POST["id"])
					return update_score(gs.user1,gs,gs.user2,request)
				else:
					# timeout to allow for starting time differences
					wait = gs.u2start - gs.u1start
					return HttpResponse(str(wait.total_seconds()))
			else:
				# calculate which user was faster
				if gs.u1start > gs.u2start:
					gs.u1end = datetime.datetime.now()
					gs.save()
					gs = GameSession.objects.get(id = request.POST["id"])
					if ((gs.u1start - gs.u2start) > (gs.u1end - gs.u2end)):
						return update_score(gs.user1,gs,gs.user2,request)
					else:
						return update_score(gs.user2,gs,gs.user1,request)
				return HttpResponse("You have been defeated !")
		else:
			# if this is the second time the user calls this request, it means
			# that the other user has timed out and cannot possibly win
			return update_score(gs.user1,gs,gs.user2,request)
	else:
	# case session.user2 finished typing, same cases but reversed. - I saw no way of 
	# halfing the size of this function apart from passing 4+ parameters to it, which I 
	# chose not to do.
		if (gs.u2end is None):
			if (gs.u1end is None):
				gs.u2end = datetime.datetime.now()
				gs.save()
				if gs.u2start > gs.u1start:
					gs = GameSession.objects.get(id = request.POST["id"])
					return update_score(gs.user2,gs,gs.user1,request)
				else:
					wait = gs.u1start - gs.u2start
					return HttpResponse(str(wait.total_seconds()))
			else:
				if gs.u2start > gs.u1start:
					gs.u2end = datetime.datetime.now()
					gs.save()
					gs = GameSession.objects.get(id = request.POST["id"])
					if ((gs.u2start - gs.u1start) > (gs.u2end - gs.u1end)):
						return update_score(gs.user2,gs,gs.user1,request)
					else:
						return update_score(gs.user1,gs,gs.user2,request)
				return HttpResponse("You have been defeated !")
		else:
			return update_score(gs.user2,gs,gs.user1,request)

def new_wpm(up,end,start,penalty):
	return ((up.wpm + (480/(end-start).total_seconds()))/(up.games*penalty))

# updates the user profiles with their new wpm/games played and wins
def update_score(winner,gs,loser,request):
	wup = UserProfile.objects.get(user = winner)
	lup = UserProfile.objects.get(user = loser)
	wup.games = wup.games + 1
	lup.games = lup.games + 1
	wup.wins = wup.wins + 1
	if wup == gs.user1:
		if (gs.u1end is None):
			# We penalise losers by assigning them half the typing speed of the winner
			wup.wpm = new_wpm(wup,gs.u2end,gs.u2start,2)
		else:	
			wup.wpm = new_wpm(wup,gs.u1end,gs.u1start,1)
		if (gs.u2end is None):
			lup.wpm = new_wpm(lup,gs.u1end,gs.u1start,2) 
		else:
			lup.wpm = new_wpm(lup,gs.u2end,gs.u2start,1)
	else:
		if (gs.u2end is None):
			wup.wpm = new_wpm(wup,gs.u1end,gs.u1start,2)
		else:
			wup.wpm = new_wpm(wup,gs.u2end,gs.u2start,1)
		if (gs.u1end is None):
			lup.wpm = new_wpm(lup,gs.u2end,gs.u2start,2)
		else:
			lup.wpm = new_wpm(wup,gs.u1end,gs.u1start,1)
	wup.save()
	lup.save()
	gs.delete()
	if request.user == wup.user:
		return HttpResponse("You win !")
	return HttpResponse("You lose !")	
