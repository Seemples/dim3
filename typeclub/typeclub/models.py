from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.db.models import permalink


class UserProfile(models.Model):
	# This field is required
	user = models.OneToOneField(User)
	# These fields are optional
	website = models.URLField(blank=True)
	picture = models.ImageField(upload_to = 'imgs', default = 'imgs/default_profile.jpg')
	wins = models.IntegerField(blank=True, default=0)
	games = models.IntegerField(blank=True, default=0)
	wpm = models.IntegerField(blank=True, default=0)
	
	def __unicode__(self):
		return self.user.username

class GameSession(models.Model):
	user1 = models.ForeignKey(User, related_name='user1', unique = True)
	# Must have blank = True and null = True to allow foreign keys to be optional
	user2 = models.ForeignKey(User, related_name='user2', blank = True, null = True)
	words = models.TextField(blank = True)
	u1start = models.DateTimeField(blank = True, null = True)
	u2start = models.DateTimeField(blank = True, null = True)
	u1end = models.DateTimeField(blank = True, null = True)
	u2end = models.DateTimeField(blank = True, null = True)
	lock = models.BooleanField()

class UserForm(forms.ModelForm):
	confirm_password = forms.CharField(widget=forms.PasswordInput())
	password = forms.CharField(widget=forms.PasswordInput())
	class Meta:
		model = User
		fields = ["username", "email", "password"]

class UserProfileForm(forms.ModelForm):
	class Meta:
		model = UserProfile
		fields = ["website", "picture"]

class Blog(models.Model):
    title = models.CharField(max_length=100, unique=True)
    body = models.TextField()
    posted = models.DateTimeField(db_index=True, auto_now_add=True)

    def __unicode__(self):
        return '%s' % self.title

