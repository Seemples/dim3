from django.conf.urls import patterns, url
from typeclub import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^about/', views.about, name='about'),
	url(r'^register/$', views.register, name = "register"),
	url(r'^login/$', views.user_login, name = "login"),
	url(r'^logout/$', views.user_logout, name = "logout"),
	url(r'^howto/', views.howto, name = "howto"),
	url(r'^profile/$', views.profile, name = "profile"),
	url(r'^play/$', views.play, name = "play"),
	url(r'^generate_words/$', views.generate_words, name = "generate_words"),
	url(r'^check/$', views.check, name = "check"),
	url(r'^find_game/$', views.find_game, name = "find_game"),
	url(r'^join_gamesession/$', views.join_gamesession, name = "join_gamesession"),
	url(r'^leave_game/$', views.leave_game, name = "leave_game"),
	url(r'^poll/$', views.poll, name = "poll"),
	url(r'^favicon\.ico$', 'django.views.generic.simple.redirect_to', {'url': '/static/imgs/favicon.ico'})
)
