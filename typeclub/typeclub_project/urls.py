from django.conf.urls import patterns, include, url
from django.contrib import admin
from typeclub_project.settings import DEBUG
from typeclub_project.settings import MEDIA_ROOT



# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^', include('typeclub.urls')),

    # Examples:
    # url(r'^$', 'typeclub_project.views.home', name='home'),
    # url(r'^typeclub_project/', include('typeclub_project.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

if DEBUG:
	urlpatterns += patterns('django.views.static', (r'media/(?P<path>.*)', 'serve', {'document_root': MEDIA_ROOT}), )
