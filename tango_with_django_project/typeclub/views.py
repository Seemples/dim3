# Create your views here.
from django.http import HttpResponse
from django.template import RequestContext, loader
from typeclub.models import GameSession
from django.shortcuts import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from typeclub.models import UserProfile
from typeclub.models import UserForm, UserProfileForm
from django.http import HttpResponseRedirect
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

from typeclub.models import Blog, BlogCategory
from django.shortcuts import render_to_response, get_object_or_404


import datetime;
import os;
import random;

def index(request):
	return render(request, "typeclub/index.html", {"posts": Blog.objects.all()[:5]})

def about(request):
	return render(request, "typeclub/about.html", {})

def register(request):
	context = RequestContext(request)
	registered = False
	if request.method == "POST":
		uform = UserForm(data = request.POST)
		pform = UserProfileForm(request.POST, request.FILES)
		if uform.is_valid() and pform.is_valid():
			user = uform.save()
			pw = user.password
			user.set_password(pw)
			user.save()
			profile = pform.save(commit = False)
			profile.user = user
			profile.save()
			#save_file(request.FILES['picture'])
			registered = True
		else:
			print uform.errors, pform.errors
	else:
		uform = UserForm()
		pform = UserProfileForm()
	return render_to_response("typeclub/register.html", {"uform": uform, "pform": pform, "registered": registered}, context)

def save_file(file, path = ""):
	filename = file._get_name()
	fd = open('%s/%s' % (settings.MEDIA_ROOT, str(path) + str(filename)), 'wb' )
	for chunk in file.chunks():
		fd.write(chunk)
	fd.close()

def user_login(request):
	context = RequestContext(request)
	#print request
	if request.method == "POST":
		username = request.POST["username"]
		password = request.POST["password"]
		user = authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				login(request, user)
				# Redirect to page requested (default is "/")
				return HttpResponseRedirect(request.POST["next"])
			else:
				# Return a "disabled account" error message
				return HttpResponse("Your account is disabled")
		else:
			# Return an invalid login error message
			print "Invalid login details " + username + " " + password
			return render_to_response("typeclub/login.html", {}, context)
	else: 
		n = None
		if "next" in request.GET:
			n = request.GET["next"]
		# The login is a GET request, just show the user the login form
		return render_to_response("typeclub/login.html", {"next": n}, context)

@login_required
def user_logout(request):
	context = RequestContext(request)
	logout(request)
	# Redirect
	return HttpResponseRedirect("/")

def howto(request):
	return render(request, "typeclub/howto.html", {})

@login_required
def profile(request):
	context = RequestContext(request)
	u = User.objects.get(username = request.user)
	try:
		up = UserProfile.objects.get(user = u)
		tt = UserProfile.objects.all().order_by('wpm').reverse()
		tr = calculate_rank(up,tt)
		tt = tt[:10]		
	except:
		up = None
		tt = None
		tr = None
	return render_to_response("typeclub/profile.html", {"user":u, "userprofile": up, 
				  "toptype": tt, "typerank": tr}, context)

def calculate_rank(user,data):
	if len(data)>0:
		if data[0]==user:
			return 1
	count = 1
	i = 0
	while i<len(data)-1:
		if data[i].wpm != data[i+1].wpm:
			count+=1
		if data[i+1]==user:
			return count
		i+=1
	return None 

@login_required
def play(request):
	template = loader.get_template("typeclub/play.html")

	# If user1 is requesting to create a gamesession but one already exists
	if (GameSession.objects.filter(user1 = User.objects.get(username = request.user)).exists()):
		return render(request, "typeclub/find_game.html", {})

	# If user2 is requesting to join a gamesession he is in
	if (GameSession.objects.filter(user2 = User.objects.get(username = request.user)).exists()):
		gs = GameSession.objects.get(user2 = User.objects.get(username = request.user))
		return render(request, "typeclub/play.html", {"gs": gs})

	# Request must then be user1 looking to create a gamesession
	new = GameSession(user1 = User.objects.get(username = request.user))
	new.save()
	context_dict = {"gs": new, "user": request.user}
	context = RequestContext(request, context_dict)
	return HttpResponse(template.render(context))

# Either user1 or user2 can call poll
# When user 2 calls, need to stop user1 polling further and begin the game
@csrf_exempt
@login_required
def poll(request):
	# user1
	if (GameSession.objects.filter(user1 = User.objects.get(username = request.user)).exists()):
		gs = GameSession.objects.get(user1 = User.objects.get(username = request.user))
		if (gs.user2 is None):
			return HttpResponse("nr")
		return HttpResponse("r")
	# user2, starting game
	if (GameSession.objects.filter(user2 = User.objects.get(username = request.user)).exists()):
		return HttpResponse("r")
	# GameSession may have been deleted
	return HttpResponse("na")

@login_required
def find_game(request):
	template = loader.get_template('typeclub/find_game.html')

	# If request.user already is in a lobby, deletes the game session so they can join another game
	if (GameSession.objects.filter(user1 = User.objects.get(username = request.user)).exists()):
		gs = GameSession.objects.get(user1 = User.objects.get(username = request.user))
		gs.delete()

	# Returns a list of possible games to join
	gs = GameSession.objects.filter()
	context_dict = {'games': gs}
	context = RequestContext(request, context_dict)
	return HttpResponse(template.render(context))

@csrf_exempt
@login_required
def join_gamesession(request):
	if request.method == "POST":
		# Must be get, if using filter cannot update!
		gs = GameSession.objects.get(id = request.POST["id"])
		user = User.objects.get(username = request.user)
		if gs.user2 is not None:
			return render(request, "typeclub/find_game.html")
		gs.user2 = user
		gs.save()
		return HttpResponseRedirect("/play/")
	return HttpResponse("BAD")

@csrf_exempt
@login_required
def leave_game(request):
	print "hi"
	context = RequestContext(request)
	if request.method == "POST":
		sid = request.POST['id']	
		print sid
		su1 = request.POST['u1']
		print su1
		su2 = request.POST['u2']
		print su2
		user = request.user
		print user
		print su1 == user.username
		print su2 == user.username
		gs = GameSession.objects.get(id=sid)
		if su1 == user.username:
			gs.delete()
		if su2 == user.username:
			gs.user2 = None
			gs.save()
	return HttpResponseRedirect("/")

@csrf_exempt
def generate_words(request):
	if (request.method == "POST"):
		# Lock so only one user generates the words and saves, other user waits till the words are generated
		while(GameSession.objects.get(id = request.POST["id"]).lock == True):
			continue
		getGs = GameSession.objects.get(id = request.POST["id"])
		getGs.lock = True
		getGs.save()
		# Store user1 time
		if (getGs.user1 == request.user):
			getGs.u1start = datetime.datetime.now()
			getGs.u1Status = 'P'
		# Store user2 time
		else:
			getGs.u2start = datetime.datetime.now()
			getGs.u2Status = 'P'
		# First user that gets here, generate the words
		if (getGs.words == ""):
			words = [line.strip() for line in open(os.getcwd()+'/static/enable1.txt')]
			dingledongs=""
			for i in range(8):
				dingledongs+=random.choice(words)
				if (i != 7):
					dingledongs += " "
			getGs.words = dingledongs
			getGs.lock = False
			getGs.save()
			return HttpResponse(getGs.words)
		# Second user just gets the words generated
		else:	
			getGs.lock = False
			getGs.save()
			return HttpResponse(getGs.words)
	return

@csrf_exempt
def check(request):
	if request.is_ajax():
		if request.method == "POST":
			getGs = GameSession.objects.filter(id = request.POST["id"])
			if ((getGs is None) or (not getGs)):
				return HttpResponse("Game lost")
			print getGs
			words = getGs[0].words
			userWords = request.POST["textBox"].rstrip()
			if (words == userWords):
				return establish_winner(request)
			else:
				return HttpResponse("Spelling error")
	return HttpResponse("BAD REQUEST")


def establish_winner(request):
	gs = GameSession.objects.get(id = request.POST["id"])
	if ((gs.u1start is None) or (gs.u2start is None)):
		return HttpResponse("Error starting timer in game")
	if request.user == gs.user1:	
		gs.u1Status = 'S'
		gs.save()
		if (gs.u1end is None):
			if (gs.u2end is None):
				gs.u1end = datetime.datetime.now()
				gs.save()
				if gs.u1start > gs.u2start:
					gs = GameSession.objects.get(id = request.POST["id"])
					return update_score(gs.user1,gs,gs.user2,request)
				else:
					wait = gs.u2start - gs.u1start
					return HttpResponse(str(wait.total_seconds()))
			else:
				if gs.u1start > gs.u2start:
					gs.u1end = datetime.datetime.now()
					gs.save()
					gs = GameSession.objects.get(id = request.POST["id"])
					if ((gs.u1start - gs.u2start) > (gs.u1end - gs.u2end)):
						return update_score(gs.user1,gs,gs.user2,request)
					else:
						return update_score(gs.user2,gs,gs.user1,request)
				return HttpResponse("You have been defeated !")
		else:
			return update_score(gs.user1,gs,gs.user2,request)
	else:
		gs.u2Status = 'S'
		gs.save()
		if (gs.u2end is None):
			if (gs.u1end is None):
				gs.u2end = datetime.datetime.now()
				gs.save()
				if gs.u2start > gs.u1start:
					gs = GameSession.objects.get(id = request.POST["id"])
					return update_score(gs.user2,gs,gs.user1,request)
				else:
					wait = gs.u1start - gs.u2start
					return HttpResponse(str(wait.total_seconds()))
			else:
				if gs.u2start > gs.u1start:
					gs.u2end = datetime.datetime.now()
					gs.save()
					gs = GameSession.objects.get(id = request.POST["id"])
					if ((gs.u2start - gs.u1start) > (gs.u2end - gs.u1end)):
						return update_score(gs.user2,gs,gs.user1,request)
					else:
						return update_score(gs.user1,gs,gs.user2,request)
				return HttpResponse("You have been defeated !")
		else:
			return update_score(gs.user2,gs,gs.user1,request)


def update_score(winner,gs,loser,request):
	print "update_score"
	wup = UserProfile.objects.get(user = winner)
	lup = UserProfile.objects.get(user = loser)
	wup.games = wup.games + 1
	lup.games = lup.games + 1
	wup.wins = wup.wins + 1
	if wup == gs.user1:
		if (gs.u1end is None):
			wup.wpm = (wup.wpm + (480/(gs.u2end-gs.u2start).total_seconds()))/(wup.games*2)
		else:	
			wup.wpm = (wup.wpm + (480/(gs.u1end-gs.u1start).total_seconds()))/wup.games
		if (gs.u2end is None):
			lup.wpm = (lup.wpm + (480/(gs.u1end-gs.u1start).total_seconds()))/(lup.games*2) 
		else:
			lup.wpm = (lup.wpm + (480/(gs.u2end-gs.u2start).total_seconds()))/lup.games
	else:
		if (gs.u2end is None):
			wup.wpm = (wup.wpm + (480/(gs.u1end-gs.u1start).total_seconds()))/(wup.games*2)
		else:
			wup.wpm = (wup.wpm + (480/(gs.u2end-gs.u2start).total_seconds()))/wup.games
		if (gs.u1end is None):
			lup.wpm = (lup.wpm + (480/(gs.u2end-gs.u2start).total_seconds()))/(lup.games*2)
		else:
			lup.wpm = (lup.wpm + (480/(gs.u1end-gs.u1start).total_seconds()))/lup.games
	wup.save()
	lup.save()
	print wup.user
	print "won"
	gs.delete()
	if request.user == wup.user:
		return HttpResponse("You win !")
	return HttpResponse("You lose !")	

def blogindex(request):
    return render_to_response('typeclub/blogindex.html', {
        'categories': BlogCategory.objects.all(),
        'posts': Blog.objects.all()[:10]
    })

def view_blogPost(request, slug):   
    return render_to_response('typeclub/view_blogPost.html', {
        'post': get_object_or_404(Blog, slug=slug)
    })

def view_blogCategory(request, slug):
    category = get_object_or_404(BlogCategory, slug=slug)
    return render_to_response('typeclub/view_blogCategory.html', {
        'category': category,
        'posts': Blog.objects.filter(category=category)[:5]
    })
