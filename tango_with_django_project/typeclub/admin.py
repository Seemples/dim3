from django.contrib import admin
from typeclub.models import UserProfile, GameSession, Blog, BlogCategory



class BlogAdmin(admin.ModelAdmin):
    exclude = ['posted']
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(UserProfile)
admin.site.register(GameSession)



admin.site.register(Blog)
admin.site.register(BlogCategory)
