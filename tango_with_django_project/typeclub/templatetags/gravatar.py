### gravatar.py ###############
### place inside a 'templatetags' directory inside the top level of a Django app (not project, must be inside an app)
### at the top of your page template include this:
### {% load gravatar %}
### and to use the url do this:
### <img src="{% gravatar_url 'someone@somewhere.com' %}">
### or
### <img src="{% gravatar_url sometemplatevariable %}">
### just make sure to update the "default" image path below

from django import template
import urllib, hashlib

register = template.Library()

class GravatarUrlNode(template.Node):
 &nbsp; &nbsp;def __init__(self, email):
 &nbsp; &nbsp; &nbsp; &nbsp;self.email = template.Variable(email)

 &nbsp; &nbsp;def render(self, context):
 &nbsp; &nbsp; &nbsp; &nbsp;try:
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;email = self.email.resolve(context)
 &nbsp; &nbsp; &nbsp; &nbsp;except template.VariableDoesNotExist:
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;return ''

 &nbsp; &nbsp; &nbsp; &nbsp;default = "http://example.com/static/images/defaultavatar.jpg"
 &nbsp; &nbsp; &nbsp; &nbsp;size = 40

 &nbsp; &nbsp; &nbsp; &nbsp;gravatar_url = "http://www.gravatar.com/avatar/" + hashlib.md5(email.lower()).hexdigest() + "?"
 &nbsp; &nbsp; &nbsp; &nbsp;gravatar_url += urllib.urlencode({'d':default, 's':str(size)})

 &nbsp; &nbsp; &nbsp; &nbsp;return gravatar_url

@register.tag
def gravatar_url(parser, token):
 &nbsp; &nbsp;try:
 &nbsp; &nbsp; &nbsp; &nbsp;tag_name, email = token.split_contents()

 &nbsp; &nbsp;except ValueError:
 &nbsp; &nbsp; &nbsp; &nbsp;raise template.TemplateSyntaxError, "%r tag requires a single argument" % token.contents.split()[0]

 &nbsp; &nbsp;return GravatarUrlNode(email)
