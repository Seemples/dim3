from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm


from django.db.models import permalink


# Create your models here.

class UserProfile(models.Model):
	# This field is required
	user = models.OneToOneField(User)
	# These fields are optional
	website = models.URLField(blank=True)
	picture = models.ImageField(upload_to = 'imgs', default = 'imgs/default_profile.jpg')
	wins = models.IntegerField(blank=True, default=0)
	games = models.IntegerField(blank=True, default=0)
	wpm = models.IntegerField(blank=True, default=0)
	
	def __unicode__(self):
		return self.user.username

class GameSession(models.Model):
	STATUSES = (
		('W','Waiting'),
		('P','Playing'),
		('S','Sent'),
		('L','Left'),
	)
	user1 = models.ForeignKey(User, related_name='user1', unique = True)
	# Must have blank = True and null = True to allow foreign keys to be optional
	user2 = models.ForeignKey(User, related_name='user2', blank = True, null = True)
	u1Status = models.CharField(max_length=1,choices=STATUSES,default='W')
	u2Status = models.CharField(max_length=1,choices=STATUSES,default='W')
	words = models.TextField(blank = True)
	u1start = models.DateTimeField(blank = True, null = True)
	u2start = models.DateTimeField(blank = True, null = True)
	u1end = models.DateTimeField(blank = True, null = True)
	u2end = models.DateTimeField(blank = True, null = True)
	lock = models.BooleanField()

class UserForm(forms.ModelForm):
	confirm_password = forms.CharField(widget=forms.PasswordInput())
	password = forms.CharField(widget=forms.PasswordInput())
	class Meta:
		model = User
		fields = ["username", "email", "password"]

class UserProfileForm(forms.ModelForm):
	class Meta:
		model = UserProfile
		fields = ["website", "picture"]



class Blog(models.Model):
    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True)
    body = models.TextField()
    posted = models.DateTimeField(db_index=True, auto_now_add=True)

    def __unicode__(self):
        return '%s' % self.title

    @permalink
    def get_absolute_url(self):
        return ('view_blogPost', None, { 'slug': self.slug })

class BlogCategory(models.Model):
    title = models.CharField(max_length=100, db_index=True)
    slug = models.SlugField(max_length=100, db_index=True)

    def __unicode__(self):
        return '%s' % self.title

    @permalink
    def get_absolute_url(self):
        return ('view_blogCategory', None, { 'slug': self.slug })
