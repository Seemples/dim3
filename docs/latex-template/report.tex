\documentclass{sig-alt-release2}
\usepackage{url}
\usepackage{color}
\usepackage{graphics,graphicx}

\usepackage{epsfig}
\usepackage{epstopdf}

\usepackage{colortbl}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{ifthen}  

\begin{document}
\newcommand{\todo}[1]{\textcolor{red}{#1}}
\def\newblock{\hskip .11em plus .33em minus .07em}

\conferenceinfo{DIM3} {2013, Glasgow, UK} 
\CopyrightYear{2013}
\clubpenalty=10000
\widowpenalty = 10000

\title{{TypeClub - A Multiplayer Typing Game}}

\numberofauthors{3}
\author{
\alignauthor
Tom Whiteley, Shinyi Breslin, Victor Pantazi\\
	   \affaddr{Pteam Pterodactyl}\\
      \affaddr{Dim3}\\
      \affaddr{1005730, 1005905, 1005128}\\
             \email{\{ 1005730w, 1005905b, 1005128p\}@students.glasgow.ac.uk }
}
\maketitle

\begin{abstract}
TypeClub is a browser based, two player typing game. It allows users to compete for leaderboard positions and improve their typing skills by attempting to enter a given sequence of words faster than their opponent.
\end{abstract}

\section{Aim of Application}
The objective of TypeClub is to provide a way for users to improve their typing skills in an entertaining way, through the use of a game mechanic. Leaderboards keep track of a user's progress and position relative to other players. It is hoped that these features will help users remain motivated and continue to practice.
\subsection{Assumptions}
It is assumed that the user is viewing the application on the Firefox browser with JavaScript enabled, running on a 64-bit linux operating system for the best results. Certain functionality provided by JavaScript does not behave uniformly in other browsers as they are not universally supported.
\subsection{Design Goals and Objectives}
To create an entertaining and challenging multiplayer typing game. 
\subsection{Constraints}
\begin{itemize}
\item TypeClub was built over a period of around 5 weeks, from the original pitch on the 5th of February.
\item The team were also balancing lots of coursework for other subjects whilst developing the application.
\end{itemize}
\subsection{Functionality}
Must Have:
\begin{itemize}
\item A way for users to create or join a game
\item A leaderboard and ranking of users by words per minute (WPM) score 
\item The actual game functionality: generate words and time opponents
\end{itemize}
Should Have:
\begin{itemize}
\item A help page or tutorial to help introduce users to the game
\item A way for users to add friends
\item A blog page for site-wide announcements from the admins
\item A complex profile with multiple statistics 
\end{itemize}

\section{Client Interface}

\subsection{User Interface}

The client interface is dictated by Twitter Bootstrap[1], Cascading Style Sheets (CSS), and our Django templates. It was decided to have Bootstrap stored on the server instead of calling it upon loading the site, as this allowed for more reliability and changes within the css to be implemented. As can be noted in Diagram 4, the lobby is a variable size table with rows that can be highlighted by moving the mouse above them. Diagram 5 shows the in-game screen, where changes to the CSS such as word highlighting were added on top of Twitter Bootstrap for features such as correct and incorrect spelling indication, as can be seen from label 1 and label 2. The user has one text input box, label 3, and a status indicator, label 4, which lets the user know if he is waiting for another player to join the game or if the game has started. The profile page (Diagram 6) shows the user's personal data and statistics, label 1, as well as the leaderboard and personal rank, label 2. 

\subsection{Walkthrough}
\begin{itemize}
\item User is on front page, sees latest updates via blog posts.
\item User clicks on find game. Can now see list of available game sessions, each row highlighting blue when hovered over. Also sees the user(s) in the game session.
\item User clicks on create lobby! to create a game session for others to join.
\item User is redirected to the play page where he is notified that he is now polling and waiting for an opponent.
\item Another user joins the game session.
\item Generated words appear in middle of screen with the first word highlighted in grey.
\item User begins typing, whenever he makes a mistake the word is highlighted red, whenever he presses space and the word is correct the word is highlighted green.
\item The user finishes all the words, hits enter.
\item The user receives a message displaying whether he has won or lost.
\item The user is then redirected to the profile page after hitting ok, can then see the leaderboard, his wins, number of games played, words per minute, and profile picture.
\end{itemize}

\subsection{Dynamic Interface Components}

When the user is playing the game and entering text, the highlighted text in the centre is changing colours depending on what the user is typing. The current word the user is on is highlighted with a grey background, which changes whenever the user advances to the next word. When the user makes a mistake, the word with the mistake is highlighted red and is only removed once the user backspaces to the point where he made the mistake. When the user presses space and the word is correct, the word is highlighted green.

When the user hits enter to submit his typed words for checking, the value of the textbox (the words typed) are sent to the server to compare with the words that were saved in the appropriate game session entry in the database. The server then returns whether the user was correct or incorrect and on the client side an alert with the result - either displaying an incorrect spelling error, whether the user has won, whether the user has lost, or whether he needs to wait in order to confirm victory.

The user interface aids the user by indicating mistakes instantly and confirming correct spellings. This helps the user to quickly tell whether he or she is on the right track, without having to go through and check each character individually and find mistakes him or herself.

\subsection{User Experience}

The user interface is intuitive, with all the main parts of the website being accessible via a bar located at the top of the page. This bar is also consistently shown throughout the site. Creating and joining a game is simple, with a button being displayed below all the available games the user can join. The joinable games are shown through a table which highlight blue and changes the cursor pointer when hovered over. However, when there are many games avaialble, the user has to scroll all the way to the bottom of the page to locate the create lobby button, which could lead to confusion for new users during peak times.

When playing the game, the user feedback of when he makes a mistake or gets a word correct is clear alongside what word he is currently on. The colours are all easily associated with what they indicate (green and red being correct and wrong) aside from grey, but this is made clear at the start of the game when the initials word is highlighted grey.

\subsection{Client Side Technology}

HTML and CSS is used to structure and display information client side, with Twitter Bootstrap being used to help CSS-wise. JavaScript and JQuery are also used to dynamically manipulate the website without forcing a refresh, which can be seen when playing the game through word highlighting, or when trying to find a game and hovering over each row of the table. HTML and CSS help maintain a separation of concerns when it comes to displaying information and style. Twitter Bootstrap provides clean, tested, CSS which can be easily built upon and changed and also allows the team to focus more on developing the features of the website than the looks. JavaScript allows for dynamic content and asynchronous requests to the server to limit disruption of the user's experience, with JQuery simplifying the process greatly.
 
\section{Application Architecture}

\begin{figure}[h!]
\includegraphics[width=0.4\textwidth, height=0.4\textheight]{dim3tier.eps}
\caption{Diagram showing the 3 tiered architecture for the application}
\end{figure}
The client, accessing the application via a browser, only directly interacts with the middleware, provided by django. The client browser tier is only responsible for presenting data to the user. The database tier interacts only with django, providing the necessary data for django's logic.

\subsection{Data models}
\begin{figure}[h!]
\includegraphics[width=0.5\textwidth, height=0.35\textheight]{model.pdf}
\caption{Diagram showing the Entity Relationship model}
\end{figure}
Our main models are the UserProfile and GameSession models. The UserProfile model contains a User model foreign key created when a user registers, as well as the user's personal statistics: words per minute, games played and games won. \\ The GameSession model has two User foreign keys, used to specify which users are playing in the same game. It also holds a TextField of randomly generated words that have been sent to both users and four DateTimeFields to hold the timestamps of when each user started the game and when they finished typing the words correctly. An aditional field holds a BooleanField, used to "lock" the GameSession to prevent the users from receiving two different wordlists.  
Another model created was blog, used to hold the admin's announcements. This model is only used by the index page.

\subsection{Separation of Concerns}


Django naturally provides a separation of concern through clear distinction between server side and client side logic, whilst also separating the database. HTML and CSS also aids in separating the concerns of information and styling respectively, with JavaScript and JQuery being used to communicate between the client and the server.

\subsection{Alternatives}

Instead of Django, PHP could have been an option however PHP would of probably been a lot harder to begin working with. Django is a framework, uses python (which all members of our team have had experience with), and is extremely well documented. PHP on the other hand is a whole new language and would require learning not only web development, but another language. This is a slightly unfair comparison, with Django being a framework and PHP being a language. A better comparison would be a framework which utilizes PHP, such as CakePHP. Our decision on Django however was solidified due to the resources available to us within lectures and tutorials provided in the course.

\section{Message Parsing}
\begin{figure}[h!]
\includegraphics[width=0.4\textwidth, height=0.4\textheight]{sequencediag.eps}
\caption{Diagram showing the message transactions during the game session}
\end{figure}
%%% Describe messages that are parsed back and forth
%%% For the main transactions - describe payload of the messages
%%% What are the contents of the messages, include smaple of one or two messages
%%% Format
%%% Why this format
%%% What other formats could be used, advantages and disadvantages of these formats

To start looking for a game the user requests find\_game.html from the server which returns the template alongside information about which games the user can join currently. This includes games which have been created by other users and are currently polling (waiting for another user to join) as well as games that are currently ongoing, which should be removed in future iterations due to redundancy. Creating a game session is done by pressing the ``Create Lobby" button, which sends a html request for play.html. The server then uses the request information (username) to create a new game session. The play.html template is then sent back to the user.

There are two types of clients in our implementation, user1 and user2. User1 is always the user who creates the game session whilst user2 is the user who joins the game session. Each user follows the same sequence flow, however the server responds differently depending on which user the client is. The server recognises which user is which by setting user1 to the user who executes create\_gamesession and user2 to the user who executes join\_gamesession (similar function to create\_gamesession however the request for play.html also includes the gamesession id to join).

Within play, the user begins polling (an AJAX POST request) which the server replies to using HttpResponse(message), where message is the current status of user2. This can either be ``r", ``nr", or ``na" where ``r" represents ``ready", occurring when a user2 has joined the game session, notifying the user to proceed to generating the words. ``nr" represents ``not ready" in which case continue polling, and ``na" represents ``not applicable" which occurs if the game session has been deleted, in which case the user is redirected back to find game. When a second user reaches the polling section, the user is added to the game session which changes the response for both user1 and user2 to ``r".

After receiving the ready message, both clients run generate\_words() which sends an AJAX POST request for a string of words to be used in the game. Only one string is generated and set in the GameSession object. This string is passed via HttpResponse(message) where message contains words separated by spaces. The client then parses this string and displays for the user to type. This format was used due to the simplicity of splitting this string based on the separator (using string.split(`` ")) and to alter the html content based on the produced array. XML or JSON could of been used instead of a string for the AJAX POST response, however we opted for the simplicity of using a string which we could easily implement due to the POST responses being relatively simple (a list of words or a status update). This design decision could lead to complexities later on however as parsing a string for information is more complex than parsing XML or JSON.

During the game, users press enter to signal they have finished typing the words, which triggers another AJAX POST request. The server now checks if the user has entered the words correctly. If the user has a spelling mistake, the server replies with a HttpResponse("Spelling error"). If the user has correctly entered the list of words, then the server will either respond with a time\_to\_wait message (due to the nature of networking, we cannot guarantee both clients receive the wordlist at the same moment, so time\_to\_wait is used to balance the timing offset) or with a string saying if the user has won or lost the game.

\section{Implementation Notes}

%%% Views - main views, what do they do?
%%% URL mapping schema - URL mapping and schema?
%%% External services - what external services does your application include and what handlers did  you include?
%%% Functionality checklist - which functionality is completed
%%% Known issues - What kind of works, what kind of errors do you get
%%% What technologies have been used and are required for the application. Include a list or table of all the technologies, standards, and protocols that will be required

\subsection{Main views}

The generate\_words view is used to return a string of words for the client to display and type out, and returning the correct string of words produced unexpected problems which had to be solved. We approached generate\_words incrementally, starting with returning a string of eight random words. This was then built upon to include giving the same string for users in the same game session. This led to a problem where one user would generate the words, then the second user would generate another batch of words resulting in an unfair matchup. This was solved through the use of a simple mutex, which is a value in the game session indicating whether it is in use or not. The general workflow then became acquiring the lock, checking whether there was a string of words, generating and saving a string of words if not, and finally releasing the lock and returning the string. Another way of solving this problem that was considered was creating the string of words upon creation of the game session, and generate\_words only returning the string instead of being given the responsibility of generation as well. However, we opted for the former due to wanting to implement word generation based on skill level in the future, which cannot be done on game session creation as the second user would not be known. Although, in hindsight, this would not have been a problem if we changed the model to store two strings instead of one shared string.

The check view is used to check if the user typed in the words correctly. If he has, it then calls establish\_winner(request). This function has the neccessary logic to set the end time for the user and then decide which user has won, or if a waiting period is needed to complete the calculation, responding accordingly. The function update\_score(winnerprofile,gamesession,loserprofile) is then called, updating the user profile data of both users with the results of the match, deleting the gamesession object from the database and replying to the clients with their results.

\subsection{URL Mapping Schema}

Each URL points to the relevant page or view, with no URL modifiers used as all game sessions / profile information requests are handled with the information passed in via the request, i.e. the game session ID would be passed in alongside a POST request to join\_gamesession, to indicate what game session the current user wishes to join.

All URLs are also easily understandable and easy to remember, for example: ``www.typeclub.me.uk/find\_game" will take the user to the find game page where he or she can then join a game.

\subsection{External Services}

The only are no external services used by our system. 

\subsection{Functionality Checklist}

\begin{itemize}
\item User registration, logging in, logging out
\item Uploading user profile picture
\item Blog creation and viewing these updates on the front page
\item Creating a game session
\item Joining a game session
\item Displaying joinable game sessions
\item Generating and displaying words
\item Word highlighting including current word, correctly typed words, and incorrectly typed words
\item Server-side checking of user's typing
\item Leaderboard displaying users with highest words per minute
\item Displaying user's rank in leaderboard
\item Viewing user profile with above statistics
\end{itemize}

\subsection{Known Issues}

\begin{itemize}
\item Leaving a game session halfway through a game, currently the only way to remove a game session is by user1 navigating to the find game page. The current system logic also does not account for user2 leaving the game.
\item Word highlighting can be buggy due to the keyboard press handler's logic being based around counting the number of characters entered, which results in incorrect highlighting as the system tries to compare wrong characters together whenever the user either navigates to earlier typed words with the mouse and entering characters, or using keyboard shortcuts. This can be fixed easily however by basing the current character and word in a similar way to how backspace works (which has a different handler), which uses the current carot position instead of counting.
\item Games which are unjoinable being displaying on the find game page, such as games which are full.
\item Cropping the user profile picture to a more space-friendly size.
\item Words per minute logic incorrect, currently has wrong arithmetic.
\item Lock on game session can sometimes fail due to one user acquiring the lock, but not notifying the database quick enough.
\item Datetime stamp on when the user beings sometimes not being stored due to a broken pipe error (errno 32).
\item Redirecting after completing a game can fail due to different browser reactions, e.g. chrome does not redirect but firefox does.
\end{itemize}

\subsection{Technologies Used}

\begin{itemize}
\item HTTP - Protocol used for server/client communication
\item HTML - Displaying templates client side
\item CSS - Styling templates
\item JavaScript - Client side scripting
\item JQuery - Simplifying client side scripting
\item Twitter Bootstrap - Aiding CSS
\item Django - Implementation of server side
\item Python - Used by Django
\item AJAX - Used for asynchronous scripting, not requiring page to be reloaded when requesting information from server
\item SQLite3 - Database
\end{itemize}

\section{Reflective Summary}
First and foremost we have learnt the benefits of using a web framework. The ability to quickly get results and make modifications is very useful, and with the addition of other technologies like the twitter bootstrap CSS, it's very easy to put out a reasonably useable, and decent looking site in a very short time frame.

Django gave us a simple way to generate a lot of content with little effort; Managing the database in particular was made simple through the inbuilt admin panel and the model system for creating tables.
Twitter Bootstrap CSS has allowed us to create pages that look great with no effort at all, and using sites like Bootswatch, there is a great amount of flexibility available in the actual look and feel of the site on top of the CSS framework they provide.

Our main problem was developing code to manage multiplayer sessions. Although the individual players do not need to be directly synchronised, the server does need to keep track of whether or not they have disconnected from the game, or submitted their words. This proved to be quite challenging, as there are no easy to use multiplayer session APIs available that do not have large numbers of dependencies or superfluous features. The easiest method was to implement our own, which took a large amount of the overall development time to complete, and raised a few issues involving compatibility with different browsers and standards. The session coding was the most challenging part of development, as keeping track of two users simultaneously required the use of continuous polling and the useage of timestamps to make sure to accommodate for delays in start times, disconnections, or other anomalies resulting from the somewhat uncertain nature of web browsing.

Getting this working is by far our greatest achievement, and we have established a solid base from which we can comfortably expand to include more polish and features.
\section{Summary and Future Work}
\subsection{Summary}
Currently the application allows users to register, log in and out, create and join sessions, and play the game with mostly functional word highlighting. The front page has a blog which can be posted to by anyone with admin privileges, and the profile page provides player statistics and a leaderboard. based on previous games played. The generation of Words Per Minute is currently not functioning as intended, and there are a few edge cases for joining/leaving games that cause sessions to break.
\subsection{Future Work}
There are a few things that we would do to improve the application given more time to work on it.
\begin{itemize}
\item Fix existing bugs\\
Through the implementation process and demonstration of the application, certain bugs were identified, and the first step to be taken in moving forward with TypeClub would be to fix these, and bring what we have already created up to a higher standard of polish, to better make way for future additions.
\item More registration and user management options\\
adding support for OAuth would give users an easy way to access the site and gives the convenience of using their existing credentials from other supported websites. Adding a page to modify user details such as email, password, and display picture after registration would also be a good feature to round out the functionality.
\item Better support for user statistics\\
The features that felt most neglected in the end were the ones providing feedback to players regarding their own progress. Whilst a leaderboard was provided listing the top typists, ideally multiple leaderboards should be available to list players with the most wins, most games played, and their position with respect to friends. This of course would require a friends list, which is another feature that was hoped to be provided, but could easily be added at a later stage.
\item Better realtime feedback during gameplay\\
Something that was mentioned during the presentation was that it would be nice to provide information about how your opponent was progressing while playing the game. Displaying the words they have completed, or even keeping track of each character as it is typed would make the multiplayer aspect feel more complete, and players would feel less disconnected from their opponent. Another useful addition would be enhanced word highlighting, showing only characters starting from the error in red, to give a better idea of when and where mistakes are made.
\item Matchmaking and ranking\\
The ability to immediately join a game without browsing the session list is something that we would aim to include in a future version. The process of matchmaking to enable this would most likely involve the inclusion of ranking, probably using an ELO calculation. This would allow players of similar ranks to easily find games with each other. Another solution would be to present different sets of words to each player based on their rank, allowing players of lower skill to play with friends of higher skill, while still experiencing a level of difficulty appropriate for their rank.
\item New game modes and wordlists\\
The Enable wordlist currently being used to generate the session words is designed for scrabble players; this means it includes a large number of long, confusing words meant to allow players to maximise the number of points they can get when playing such games. Ideally we would like to provide a selection of words on a sliding scale of difficulty, shorter, more common words for lower ranked players, and longer, obscurer words for advanced players. Moreover, users could potentially provide their own word lists, for a customisable experience. A few new game modes that could be included follow.\\
Follow the Leader: One player inputs words at their own pace, whilst other connected players attempt to keep up. This allows players to spend as long as they wish typing with eachother, and can ramp up or slow the challenge as they please based on the speed at which the pacesetter types.\\
Massive Multiplayer Games: Currently sessions are restricted to two players. In the future, this could be expanded to allow many more players to compete.\\
Attack/Defend: Initially we had planned to include phases in the game; one player attacks, the other defends. Each player has a health bar, denoting the number of rounds they can lose before the game ends. If the attacker finishes the round first, the defender loses a point of health, if the defender finishes first, they successfully 'block' the attack. This would be accompanied by graphics of characters that could be customised by winning games and increasing ranks, providing a psuedo-role-playing-game element to TypeClub.
\end{itemize}
\section{Acknowledgements}
Our thanks to the lecturers and demonstrators for their comments and suggestions. And our thanks to the peer reviewers, and the industry judges for their feedback and interest.\newpage
\appendix
\begin{figure}[h!]
\includegraphics[width=1\textwidth,height=2\textheight,keepaspectratio=true]{diagrams/game.png}
\caption{Diagram showing the game screen}
\end{figure}
\clearpage
\begin{figure}[h!]
\includegraphics[width=1\textwidth,height=2\textheight,keepaspectratio=true]{diagrams/lobby.png}
\caption{Diagram showing the lobby page}
\end{figure}
\clearpage
\begin{figure}[h!]
\includegraphics[width=1\textwidth,height=2\textheight,keepaspectratio=true]{diagrams/profile.png}
\caption{Diagram showing the user's profile page}
\end{figure}
\bibliographystyle{abbrv}
\bibliography{sig-proc}

\section{References}
[1] Twitter Bootstrap. http://twitter.github.com/bootstrap/.

\end{document}
